var queryInfo = {
    active: true,
    currentWindow: true
}

var menuItem = {
    id: "priceDrop",
    title: "Price Drop | Add to watchlist"
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

function logger(mesg) {
    console.log('['+ new Date().toUTCString() + '] ' + mesg)
}

// chrome.storage.sync.remove(['pditmlst'])

chrome.contextMenus.create(menuItem)
chrome.contextMenus.onClicked.addListener(function (clickData) {
    if (clickData.menuItemId == 'priceDrop') {
        let mesg = {
            text: "NewItem"
        }
        chrome.tabs.query(queryInfo, function (tabs) {
            chrome.tabs.sendMessage(tabs[0].id, mesg, function (resp) {
                if (resp) {
                    chrome.storage.sync.get(['pditmlst'], function (items) {
                        var pditmlst = []
                        pditmlst.push(resp)
                        if (Object.keys(items).length) {
                            pditmlst.push(...items.pditmlst)
                        }

                        chrome.storage.sync.set({ pditmlst: pditmlst }, function () {
                            let notiOpt = {
                                type: "basic",
                                title: resp.Price,
                                iconUrl: resp.Image,
                                message: resp.Title
                            }
                            chrome.notifications.create("NewItemNoti" + Math.random().toString(), notiOpt)
                        })
                    })
                }
            })
        })
    }
})

function lzdGetPrice(link) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest()
        xhr.open("GET", link, true)
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                let result = xhr.responseText.trim().match(/app.run[\S\s]*\}\}\);/g)
                if(result) {
                    let r = result[0].replace("app.run(", "")
                    r = r.replace(");", "")
                    let data = JSON.parse(r)
                    resolve(data.data.root.fields.skuInfos[0].price.salePrice.text)
                } else {
                    resolve("Not Found")
                }
            }
        }
        xhr.send()
    })
}

function shpeGetPrice(link) {
    return new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest()
        let param = link.split(".")
        link = `https://shopee.co.th/api/v2/item/get?itemid=${param[param.length-1]}&shopid=${param[param.length-2]}`
        xhr.open("GET", link, true)
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                let result = JSON.parse(xhr.responseText)
                if(result) { 
                    let price = "฿" + result.item.price.toString().slice(0, -5).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,")
                    resolve(price)
                } else {
                    resolve("Not Found")
                }
            }
        }
        xhr.send()
    })
}

function GetWatchlist() {
    return new Promise((resolve, reject) => {
        chrome.storage.sync.get(['pditmlst'], function (items) {
            resolve(items.pditmlst)
        })
    })
}

function UpdateItemlist(index, title, price) {
    chrome.storage.sync.get(['pditmlst'], function (items) {
        if (items.pditmlst[index].Title == title) {
            if (items.pditmlst[index].Price != price) {
                chrome.browserAction.setBadgeText({text: "!"})
            }

            items.pditmlst[index].Price = price
            if (ToNum(items.pditmlst[index].MinPrice) > ToNum(price)) {
                items.pditmlst[index].MinPrice = price
            }

            if (ToNum(items.pditmlst[index].MaxPrice) < ToNum(price)) {
                items.pditmlst[index].MaxPrice = price
            }
        }

        chrome.storage.sync.set({ pditmlst: items.pditmlst }, function () {
            // console.log("> Updated")
        })
    })
}

function ToNum(str) {
    return Number(str.replace(/[^0-9.-]+/g,""))
}

function iSleep() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve()
        }, 1000 * 60 * 30)
        // }, 1000 * 5)
    })
}

async function Woker() {
    while (true) {
        var items = await GetWatchlist()
        if (items) {
            await asyncForEach(items, async (item, index) => {
                let price = "Not Found"
                if (item.Producer == 'Lazada') { 
                    price = await lzdGetPrice(item.Base_link)
                } else if(item.Producer == 'Shopee') {
                    price = await shpeGetPrice(item.Base_link)
                }

                if(price != "Not Found") {
                    UpdateItemlist(index, item.Title, price)
                }
                await iSleep()
            })
        }
    }
}

Woker()