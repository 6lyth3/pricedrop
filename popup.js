$(document).ready(function () {
    $('[id^="bin-"]').click(function (e) {
        var idx = parseInt($(this).attr("data-index"))
        chrome.storage.sync.get(['pditmlst'], function(resp) {
            resp.pditmlst.splice(idx, 1)
            chrome.storage.sync.set({ pditmlst: resp.pditmlst }, function () { })
            ShowItemList()
        })
        e.preventDefault()
        location.reload()
    })
    chrome.browserAction.setBadgeText({text: ""})
})

var queryInfo = {
    active: true,
    currentWindow: true
}

function ShowItemList() {
    chrome.storage.sync.get(['pditmlst'], function (resp) {
        let item_li = document.getElementById('itemList')
        let info = document.getElementById('info')
        // Clear innerHTML
        item_li.innerHTML = ""
        if (Object.keys(resp).length) {
            if(resp.pditmlst.length) {
                info.style.display = "none"
            }
            
            resp.pditmlst.forEach((item, i) => {
                let price = parseFloat(item.Price.replace(/[^0-9.-]+/g, ''))
                let oldPrice = parseFloat(item.OldPrice.replace(/[^0-9.-]+/g, ''))
                if (price > oldPrice) {
                    let percent = (((price-oldPrice)*100)/price).toFixed(2)
                    item_li.innerHTML += `<li>
       <div class='uk-card uk-card-default uk-width-1-2@m'>
           <i id="bin-${i}" class='fa fa-trash uk-float-right mg-bin uk-text-meta' data-index=${i} aria-hidden='true'></i>
           <div class='uk-card-header'>
               <div class='uk-grid-small uk-flex-middle' uk-grid>
                   <div class='uk-width-auto'>
                       <a href='${item.Ref_link}' target='_blank'>
                       <img class='img-border-8' width='60' height='60' src='${item.Image}'>
                       </a>
                   </div>
                   <div class='uk-width-expand'>
                       <h3 class='uk-card-title uk-margin-remove-bottom'><a href='${item.Ref_link}' target='_blank'>${item.Title}</a></h3>
                       <p class='uk-text-meta uk-margin-remove-top'>
                           <img src='images/${item.Producer}-icon.png' width='20' height='20'>
                           ${item.Producer} |  <i class="fa fa-tags"></i> ${item.MinPrice} - ${item.MaxPrice}
                       </p>
                   </div>
                   <div>
                       <p class='cost mb-0 red'>${item.Price}</p>
                       <p class='uk-text-meta mt-0'>${item.OldPrice} <data-p class='red'><i class='fa fa-caret-up' aria-hidden='true'></i>${percent}%</data-p></p>
                    </div> </div> </div> </div> </li>`
                } else if (price < oldPrice) {
                    let percent = (((oldPrice-price)*100)/oldPrice).toFixed(2)
                    item_li.innerHTML += `<li>
       <div class='uk-card uk-card-default uk-width-1-2@m'>
           <i id="bin-${i}" class='fa fa-trash uk-float-right mg-bin uk-text-meta' data-index=${i} aria-hidden='true'></i>
           <div class='uk-card-header'>
               <div class='uk-grid-small uk-flex-middle' uk-grid>
                   <div class='uk-width-auto'>
                       <a href='${item.Ref_link}' target='_blank'>
                       <img class='img-border-8' width='60' height='60' src='${item.Image}'>
                       </a>
                   </div>
                   <div class='uk-width-expand'>
                       <h3 class='uk-card-title uk-margin-remove-bottom'><a href='${item.Ref_link}' target='_blank'>${item.Title}</a></h3>
                       <p class='uk-text-meta uk-margin-remove-top'>
                           <img src='images/${item.Producer}-icon.png' width='20' height='20'>
                           ${item.Producer} |  <i class="fa fa-tags"></i> ${item.MinPrice} - ${item.MaxPrice}
                       </p>
                   </div>
                   <div>
                       <p class='cost mb-0 green'>${item.Price}</p>
                       <p class='uk-text-meta mt-0'>${item.OldPrice} <data-p class='green'><i class='fa fa-caret-down' aria-hidden='true'></i>-${percent}%</data-p></p>
                    </div> </div> </div> </div> </li>`
                } else {
                    item_li.innerHTML += `<li>
       <div class='uk-card uk-card-default uk-width-1-2@m'>
           <i id="bin-${i}" class='fa fa-trash uk-float-right mg-bin uk-text-meta' data-index=${i} aria-hidden='true'></i>
           <div class='uk-card-header'>
               <div class='uk-grid-small uk-flex-middle' uk-grid>
                   <div class='uk-width-auto'>
                       <a href='${item.Ref_link}' target='_blank'>
                       <img class='img-border-8' width='60' height='60' src='${item.Image}'>
                       </a>
                   </div>
                   <div class='uk-width-expand'>
                       <h3 class='uk-card-title uk-margin-remove-bottom'><a href='${item.Ref_link}' target='_blank'>${item.Title}</a></h3>
                       <p class='uk-text-meta uk-margin-remove-top'>
                           <img src='images/${item.Producer}-icon.png' width='20' height='20'>
                           ${item.Producer} |  <i class="fa fa-tags"></i> ${item.MinPrice} - ${item.MaxPrice}
                       </p>
                   </div>
                   <div>
                       <p class='cost mb-0 blue'>${item.Price}</p>
                    </div> </div> </div> </div> </li>`
                }
            })
        } else {
            info.style.display = "block"

        }
    })
}
ShowItemList()