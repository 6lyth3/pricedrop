var lazadaLink = "https://c.lazada.co.th/t/c.Gqf?url="
var shopeeLink = "https://go.ecotrackings.com/?token=peNxkDWJqaqvTolFpYeQe&url="

chrome.runtime.onMessage.addListener(worker)

function worker(request, sender, sendResponse) {
    var host = window.location.host
    var r = undefined
    if(request.text == "NewItem") {
        if (host.includes("lazada")) {
            r = lzdLookup()
        } else if (host.includes("shopee")) {
            r = shpeLookup()
        }

        if (r) {
            sendResponse(r)
        }
    }
}

//  Lazada.co.th
function lzdLookup() {
    let title = document.getElementById("module_product_title_1")
    let price = lzdGetPrice(document.getElementById("module_product_price_1").innerText)
    let img = document.getElementsByClassName("gallery-preview-panel__image")

    let link = linkSplit(img[0].baseURI)

    let item = {
        Title: title.innerText,
        Price: price,
        Image: img[0].src, 
        Base_link: link,
        OldPrice: price,
        Ref_link: lazadaLink + link,
        MinPrice: price,
        MaxPrice: price,
        Currency: 'THB',
        FlashSell: null,
        Producer: 'Lazada'
    }
    return item
}

function linkSplit(text) {
    let l = text.split(/\?/)
    return l[0]
}

function lzdGetPrice(text) {
    let l = text.split(/\n/)
    return l[0]
}


// Shopee.co.th
function shpeLookup() {
    let title = document.getElementsByClassName("qaNIZv")[0].innerText
    let price = document.getElementsByClassName("_3n5NQx")[0].innerText
    let img = document.querySelector("meta[property='og:image']").getAttribute('content')
    let link = document.querySelector("meta[property='og:url']").getAttribute('content')

    let item = {
        Title: title,
        Price: price,
        Image: img, 
        Base_link: link,
        OldPrice: price,
        Ref_link: shopeeLink + link,
        MinPrice: price,
        MaxPrice: price,
        Currency: 'THB',
        FlashSell: '',
        Producer: 'Shopee'
    }
    return item
}